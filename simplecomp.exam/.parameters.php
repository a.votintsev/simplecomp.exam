<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = Array(
    'GROUPS' => array(
        'BASE' => array(
            'NAME' => Loc::getMessage('BASE_SETTINGS_DESC'),
        ),
        'CACHE_SETTINGS' => array(
            'NAME' => Loc::getMessage('CACHE_SETTINGS_DESC'),
        ),
    ),

    'PARAMETERS' => array(
        'IBLOCK_CATALOG_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('IBLOCK_CATALOG_ID_DESC'),
            'TYPE' => 'STRING'
        ),
        'IBLOCK_NEWS_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('IBLOCK_NEWS_ID_DESC'),
            'TYPE' => 'STRING'
        ),
        'UF_CODE' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('UF_CODE_DESC'),
            'TYPE' => 'STRING'
        ),
        'CACHE_TYPE' => array(
            'PARENT' => 'CACHE_SETTINGS',
            'NAME' => Loc::getMessage('COMP_PROP_CACHE_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => array(
                'A' => Loc::getMessage('COMP_PROP_CACHE_TYPE_AUTO'),
                'Y' => Loc::getMessage('COMP_PROP_CACHE_TYPE_YES'),
                'N' => Loc::getMessage('COMP_PROP_CACHE_TYPE_NO'),
            ),
            'DEFAULT' => 'N',
            'ADDITIONAL_VALUES' => 'N',
        ),
        'CACHE_TIME' => array(
            'PARENT' => 'CACHE_SETTINGS',
            'NAME' => Loc::getMessage('COMP_PROP_CACHE_TIME'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 3600,
            'COLS' => 5,
        ),
    )
);