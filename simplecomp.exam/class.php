<?php
use Bitrix\Main\Loader;
use Bitrix\Iblock\Model\Section;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\IblockTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class SimpleComp extends CBitrixComponent {

    /**
     * Обертка над глобальной переменной
     *
     * @return mixed
     */
    private function application() {
        global $APPLICATION;
        return $APPLICATION;
    }

    /**
     * Проверка наличия модулей требуемых для работы компонента
     *
     * @return bool
     * @throws Exception
     */
    private function checkModules() {
        if (!Loader::includeModule('iblock')) {
            $this->__showError(Loc::getMessage('MODULE_IBLOCK_NOT_FOUND'));
            return false;
        }
        return true;
    }

    /**
     * Проверка существования ифоблоков
     *
     * @return bool
     */
    private function checkIblocks() {
        if (!IblockTable::getById($this->arParams['IBLOCK_CATALOG_ID'])->fetch()) {
            $this->__showError(Loc::getMessage('IBLOCK_NOT_FOUND' , ['#ID#' => $this->arParams['IBLOCK_CATALOG_ID']]));
            return false;
        } elseif (!IblockTable::getById($this->arParams['IBLOCK_NEWS_ID'])->fetch()) {
            $this->__showError(Loc::getMessage('IBLOCK_NOT_FOUND' , ['#ID#' => $this->arParams['IBLOCK_NEWS_ID']]));
            return false;
        }
        return true;
    }

    /**
     * Проверка существования пользовательского поля
     *
     * @return bool
     */
    private function checkUserField() {
        try {
            $entityCatalog = $this->getEntityByIblock($this->arParams['IBLOCK_CATALOG_ID']);
            $entityCatalog::getList(array(
                'select' => array($this->arParams['UF_CODE'])
            ));
        } catch (Throwable $data) {
            $this->__showError(Loc::getMessage('UF_NOT_FOUND' , ['#ID#' => $this->arParams['IBLOCK_CATALOG_ID'], '#CODE#' => $this->arParams['UF_CODE']]));
            return false;
        }
        return true;
    }

    /**
     * Проверяет привязку пользовательского поля указанного в настройках компонента,
     * к инфоблоку новостей указанному в настройках компонента
     *
     * @return bool
     */
    private function checkFieldBinding() {
        $field = \CUserTypeEntity::GetList(
            array(),
            array(
                'ENTITY_ID' => 'IBLOCK_' . $this->arParams['IBLOCK_CATALOG_ID'] . '_SECTION',
                'FIELD_NAME' => $this->arParams['UF_CODE']
            )
        )->Fetch();

        if ($field['SETTINGS']['IBLOCK_ID'] != $this->arParams['IBLOCK_NEWS_ID']) {
            $this->__showError(Loc::getMessage('UF_NOT_BIND' , ['#ID#' => $this->arParams['IBLOCK_NEWS_ID'], '#CODE#' => $this->arParams['UF_CODE']]));
            return false;
        }
        return true;
    }

    /**
     * Возвращает ORM сущность секций для конкретного инфоблока
     *
     * @param int $iblockId
     * @return mixed
     */
    private function getEntityByIblock(int $iblockId) {
        return Section::compileEntityByIblock($iblockId);
    }

    /**
     * Возвращает список новостей
     *
     * @return mixed
     */
    private function getNews() {
        return ElementTable::getList(array(
            'filter' => array(
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->arParams['IBLOCK_NEWS_ID'],
            ),
            'select' =>  array('ID', 'NAME', 'ACTIVE_FROM'),
        ))->fetchAll();
    }

    /**
     * Возвращает список разделов по id новости
     *
     * @param int $newsId
     * @return array
     */
    private function getSectionsByNewsId(int $newsId) {
        $entity = $this->getEntityByIblock($this->arParams['IBLOCK_CATALOG_ID']);
        $sections = $entity::getList(array(
            'filter' => array(
                'GLOBAL_ACTIVE' => 'Y',
                'ACTIVE' => 'Y',
                $this->arParams['UF_CODE'] => $newsId
            ),
            'select' => array('ID', 'NAME'),
        ))->fetchAll();

        $result = [];
        foreach ($sections as $section) {
            $result['NAMES'][] = $section['NAME'];
            $result['IDS'][] = $section['ID'];
        }

        return $result;
    }

    /**
     * Возвращает значение свойства товара
     *
     * @param int $goodId
     * @param string $propertyName
     * @return mixed
     */
    private function getPropertyValue(int $goodId, string $propertyName) {
        $property = \CIBlockElement::GetProperty($this->arParams['IBLOCK_CATALOG_ID'], $goodId, 'sort', 'asc', array('CODE' => $propertyName))->fetch();
        return $property['VALUE'];
    }

    /**
     * Возвращает список товаров
     *
     * @param array $sectionsIds
     * @return mixed
     */
    private function getGoodsBySectionsIds(array $sectionsIds) {
        $goods = ElementTable::getList(array(
            'filter' => array(
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->arParams['IBLOCK_CATALOG_ID'],
                'IBLOCK_SECTION_ID' => $sectionsIds,
            ),
            'select' =>  array('ID', 'NAME'),
        ))->fetchAll();

        foreach ($goods as &$good) {
            $good['PRICE'] = $this->getPropertyValue($good['ID'], 'PRICE');;
            $good['ARTNUMBER'] = $this->getPropertyValue($good['ID'], 'ARTNUMBER');;
            $good['MATERIAL'] = $this->getPropertyValue($good['ID'], 'MATERIAL');;
        }
        unset($good);

        return $goods;
    }

    /**
     * Возвращает отформатированный список элементов каталога
     *
     * @return mixed
     */
    private function getResult() {
        $items = $this->getNews();

        $sectionsIds = [];
        foreach ($items as &$item) {
            $sections = $this->getSectionsByNewsId($item['ID']);
            $item['SECTIONS'] = $sections['NAMES'];
            $item['GOODS'] = $this->getGoodsBySectionsIds($sections['IDS']);
            $sectionsIds = array_merge($sectionsIds, $sections['IDS']);
        }
        unset($item);

        return [
            'ITEMS' => $items,
            'SECTIONS_IDS' => array_unique($sectionsIds)
        ];
    }

    /**
     * Точка входа в компонент
     *
     * @throws Exception
     */
    public function executeComponent() {
        if ($this->checkModules() && $this->checkIblocks() && $this->checkUserField() && $this->checkFieldBinding()) {
            if ($this->startResultCache()) {
                $this->arResult = $this->getResult();

                $this->includeComponentTemplate();
            }

            $goodsQuantity = count($this->getGoodsBySectionsIds($this->arResult['SECTIONS_IDS']));
            $this->application()->setTitle(Loc::getMessage('COMPONENT_TITLE') . $goodsQuantity);
        }
    }
}
