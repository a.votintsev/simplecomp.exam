<?php
$MESS['IBLOCK_CATALOG_ID_DESC'] = 'ID инфоблока с каталогом товаров';
$MESS['IBLOCK_NEWS_ID_DESC'] = 'ID инфоблока с новостями';
$MESS['UF_CODE_DESC'] = 'Код пользовательского свойства для привязки к новостям';
$MESS['CACHE_SETTINGS_DESC'] = 'Настройки кеширования';
$MESS['BASE_SETTINGS_DESC'] = 'Основные настройки';
