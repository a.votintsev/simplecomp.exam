<?php
$MESS['MODULE_IBLOCK_NOT_FOUND'] = 'Не загружены модули необходимые для работы компонента';
$MESS['COMPONENT_TITLE'] = 'В каталоге товаров представлено товаров: ';
$MESS['IBLOCK_NOT_FOUND'] = 'Инфоблока с id #ID# не существует';
$MESS['UF_NOT_FOUND'] = 'В инфоблоке с id #ID# не существует пользовательского поля #CODE#';
$MESS['UF_NOT_BIND'] = 'Поле #CODE# не привязано к элементам инфоблока #ID#';
