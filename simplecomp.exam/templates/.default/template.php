<p class="bold_text">Каталог:</p>
<ul>
    <?foreach ($arResult['ITEMS'] as $item) {?>
        <?$strSections = implode(', ', $item['SECTIONS']);?>
        <?$date = ($item['ACTIVE_FROM'] instanceof \Bitrix\Main\Type\DateTime) ? $item['ACTIVE_FROM']->format('d.m.Y') : '';?>
        <li><span class="bold_text"><?=$item['NAME']?></span> - <?=$date?> (<?=$strSections?>)</li>
        <ul>
            <?foreach ($item['GOODS'] as $good) {?>
                <li><?=$good['NAME']?> - <?=$good['PRICE']?> - <?=$good['MATERIAL']?> - <?=$good['ARTNUMBER']?></li>
            <?}?>
        </ul>
    <?}?>
</ul>
